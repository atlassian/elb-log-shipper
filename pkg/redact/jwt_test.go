package redact

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	jwtHs256 = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
	jwtRs256 = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.EkN-DOsnsuRjRO6BxXemmJDm3HbxrbRzXglbN2S4sOkopdU4IsDxTI8jO19W_A4K8ZPJijNLis4EZsHeY559a4DFOd50_OqgHGuERTqYZyuhtF39yxJPAjUESwxk2J5k_4zM3O-vtd1Ghyo4IbqKKSy6J9mTniYJPenn5-HIirE"
	notJwt   = "aaaaaa.bbbbbb.cccccc"
)

var (
	// just padding our test strings so that they have equal length
	paddedJwtHs256 = jwtHs256 + " " + strings.Repeat("$", len(jwtRs256)-len(jwtHs256)-1)
	paddedJwtRs256 = jwtRs256
	paddedNotJwt   = notJwt + " " + strings.Repeat("$", len(jwtRs256)-len(notJwt)-1)
)

func fixtureReader(t *testing.T, s string) (io.Reader, func()) {
	file, err := os.Open(path.Join(".", "fixtures", s))
	if err != nil {
		t.Fatal(err)
	}
	return file, func() {
		file.Close()
	}
}

func fixtureString(t *testing.T, s string) string {
	reader, close := fixtureReader(t, s)
	defer close()

	buffer := bytes.Buffer{}
	_, err := buffer.ReadFrom(reader)
	if err != nil {
		t.Fatal(err)
	}
	return buffer.String()
}

func TestRedactJwts(t *testing.T) {
	t.Parallel()

	input := fixtureString(t, "01_jwt_input.txt")
	want := fixtureString(t, "01_jwt_want.txt")

	got := RedactJwts(input)

	assert.Equal(t, want, got)
}

func BenchmarkRedactJwts(b *testing.B) {
	for i := 10; i < 100000; i = i * 10 {
		func(i int) {
			var input string

			input = strings.Repeat(paddedJwtHs256, i)
			b.Run(fmt.Sprintf("JWT w/HS256: len(input)=%d", len(input)), func(b *testing.B) {
				got := RedactJwts(input)
				assert.NotEmpty(b, got)
			})

			input = strings.Repeat(paddedJwtRs256, i)
			b.Run(fmt.Sprintf("JWT w/RS256: len(input)=%d", len(input)), func(b *testing.B) {
				got := RedactJwts(input)
				assert.NotEmpty(b, got)
			})

			input = strings.Repeat(paddedNotJwt, i)
			b.Run(fmt.Sprintf("not JWT: len(input)=%d", len(input)), func(b *testing.B) {
				got := RedactJwts(input)
				assert.NotEmpty(b, got)
			})
		}(i)
	}
}
