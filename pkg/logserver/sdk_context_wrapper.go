package logserver

import (
	"context"

	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type SdkContextWrapper struct {
	ElbService     *elb.ELB
	Elbv2Service   *elbv2.ELBV2
	KinesisService *kinesis.Kinesis
	S3Service      *s3.S3
	SnsService     *sns.SNS
	SqsService     *sqs.SQS
}

func (w *SdkContextWrapper) GetObject(ctx context.Context, input *s3.GetObjectInput) (*s3.GetObjectOutput, error) {
	request, output := w.S3Service.GetObjectRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) Subscribe(ctx context.Context, input *sns.SubscribeInput) (*sns.SubscribeOutput, error) {
	request, output := w.SnsService.SubscribeRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) GetQueueAttributes(ctx context.Context, input *sqs.GetQueueAttributesInput) (*sqs.GetQueueAttributesOutput, error) {
	request, output := w.SqsService.GetQueueAttributesRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) ReceiveMessage(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	request, output := w.SqsService.ReceiveMessageRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) DeleteMessage(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	request, output := w.SqsService.DeleteMessageRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) DescribeTagsElb(ctx context.Context, input *elb.DescribeTagsInput) (*elb.DescribeTagsOutput, error) {
	request, output := w.ElbService.DescribeTagsRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) DescribeTagsElbv2(ctx context.Context, input *elbv2.DescribeTagsInput) (*elbv2.DescribeTagsOutput, error) {
	request, output := w.Elbv2Service.DescribeTagsRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func (w *SdkContextWrapper) PutRecord(ctx context.Context, input *kinesis.PutRecordInput) (*kinesis.PutRecordOutput, error) {
	request, output := w.KinesisService.PutRecordRequest(input)
	return output, sendWrappedRequest(ctx, request)
}

func sendWrappedRequest(ctx context.Context, req *request.Request) error {
	req.HTTPRequest = req.HTTPRequest.WithContext(ctx)
	return req.Send()
}
