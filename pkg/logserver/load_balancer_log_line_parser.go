package logserver

import (
	"errors"
	"regexp"
	"strconv"
	"strings"

	log "github.com/Sirupsen/logrus"
)

const (
	logLineFormat = `^(?:(?P<connection_type>http|https|h2|ws|wss) )?(?P<timestamp>[^ ]+) [^ ]+ (?P<client_raw>(?P<client_ip>[^ ]+):(?P<client_port>[^ ]+)) (?P<target_raw>(?P<target_ip>[^ ]+):(?P<target_port>[^ ]+)|[^: ]+) (?P<request_processing_time>[^ ]+) (?P<target_processing_time>[^ ]+) (?P<response_processing_time>[^ ]+) (?P<load_balancer_status_code>[^ ]+) (?P<target_status_code>[^ ]+) (?P<received_bytes>[^ ]+) (?P<sent_bytes>[^ ]+) "(?P<request_method>[^" ]+) (?P<request_url>[^ ]+) (?P<request_http_version>[^"]*?)\s?" "(?P<user_agent>(?:[^"]|\")*)" (?P<ssl_cipher>[^ ]+) (?P<ssl_protocol>[^ ]+)(?: (?:(?P<target_group_arn>[^ ]+))? "(?P<trace_id>([^"]|\")*)")?$`
)

var (
	logLineRegex = regexp.MustCompile(logLineFormat)
)

type LoadBalancerLogLineParser struct {
}

func (p *LoadBalancerLogLineParser) ParseLine(line string) (*LogLineEntry, error) {
	result := findNamedStringSubmatches(logLineRegex, strings.TrimSpace(line))
	if result == nil {
		return nil, errors.New("non conforming log line")
	}

	// Filter out omitted values for which AWS uses as a placeholder.
	for k, v := range result {
		if v == "-" || v == "-1" {
			result[k] = ""
		}
	}

	dest := &LogLineEntry{
		ConnectionType:         result["connection_type"],
		Timestamp:              result["timestamp"],
		Client:                 result["client_ip"],
		LoadBalancerStatusCode: result["load_balancer_status_code"],
		TargetStatusCode:       result["target_status_code"],
		Request: LogLineEntryRequest{
			Method:      result["request_method"],
			Url:         result["request_url"],
			HttpVersion: result["request_http_version"],
		},
		UserAgent:      result["user_agent"],
		TlsCipher:      result["ssl_cipher"],
		TlsProtocol:    result["ssl_protocol"],
		TargetGroupArn: result["target_group_arn"],
		TraceId:        result["trace_id"],
	}

	if result["request_method"] == "" {
		dest.Request.Client = "Transport"
	} else {
		dest.Request.Client = "HTTP"
	}

	if result["target_raw"] != "" {
		dest.Target = result["target_ip"]
		dest.Port = parseInt64FromMapKey(result, "target_port")
	}

	var duration float64

	if result["request_processing_time"] != "" {
		dest.RequestProcessingTime = parseFloat64FromMapKey(result, "request_processing_time")
		duration = duration + dest.RequestProcessingTime
	}
	if result["target_processing_time"] != "" {
		dest.TargetProcessingTime = parseFloat64FromMapKey(result, "target_processing_time")
		duration = duration + dest.TargetProcessingTime
	}
	if result["response_processing_time"] != "" {
		dest.ResponseProcessingTime = parseFloat64FromMapKey(result, "response_processing_time")
		duration = duration + dest.ResponseProcessingTime
	}
	dest.ReceivedBytes = parseInt64FromMapKey(result, "received_bytes")
	dest.SentBytes = parseInt64FromMapKey(result, "sent_bytes")
	dest.Duration = duration * 1000
	dest.Bytes = dest.ReceivedBytes + dest.SentBytes

	return dest, nil
}

// Assumes key exists in m.
func parseInt64FromMapKey(m map[string]string, key string) int64 {
	dest, err := strconv.ParseInt(m[key], 10, 64)
	if err != nil {
		log.WithFields(log.Fields{
			"key":   key,
			"value": m[key],
		}).Errorf("error parsing int64: %v", err)
	}
	return dest
}

// Assumes key exists in m.
func parseFloat64FromMapKey(m map[string]string, key string) float64 {
	dest, err := strconv.ParseFloat(m[key], 64)
	if err != nil {
		log.WithFields(log.Fields{
			"key":   key,
			"value": m[key],
		}).Errorf("error parsing float64: %v", err)
	}
	return dest
}
