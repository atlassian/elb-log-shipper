package logserver

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseLine(t *testing.T) {
	t.Parallel()
	parseTests := []struct {
		name      string
		line      string
		expected  *LogLineEntry
		expectErr bool
	}{
		{
			name: "ELB TCP",
			line: `2015-05-13T23:39:43.945958Z my-loadbalancer 192.168.131.39:2817 10.0.0.1:80 0.001069 0.000028 0.000041 - - 82 305 "- - - " "-" - -`,
			expected: &LogLineEntry{
				Timestamp:              "2015-05-13T23:39:43.945958Z",
				Client:                 "192.168.131.39",
				Target:                 "10.0.0.1",
				RequestProcessingTime:  0.001069,
				TargetProcessingTime:   0.000028,
				ResponseProcessingTime: 0.000041,
				Duration:               1.1380000000000001,
				Port:                   80,
				Bytes:                  387,
				ReceivedBytes:          82,
				SentBytes:              305,
				Request: LogLineEntryRequest{
					Client: "Transport",
				},
			},
		},
		{
			name: "ALB HTTP/2 over SSL/TLS with failed dispatch",
			line: `h2 2016-08-10T00:10:33.145057Z app/my-loadbalancer/abcdef1234567890 10.0.1.252:48160 - -1 -1 -1 200 200 5 257 "GET https://10.0.2.105:773/ HTTP/2.0" "curl/7.46.0" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 arn:aws:elasticloadbalancing:us-west-2:123456789012:targetgroup/my-targets/bcdef1234567890a "Root=1-58337327-72bd00b0343d75b906739c42"`,
			expected: &LogLineEntry{
				ConnectionType:         "h2",
				Timestamp:              "2016-08-10T00:10:33.145057Z",
				Client:                 "10.0.1.252",
				LoadBalancerStatusCode: "200",
				TargetStatusCode:       "200",
				Bytes:                  262,
				ReceivedBytes:          5,
				SentBytes:              257,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         "https://10.0.2.105:773/",
					HttpVersion: "HTTP/2.0",
				},
				UserAgent:      "curl/7.46.0",
				TlsCipher:      "ECDHE-RSA-AES128-GCM-SHA256",
				TlsProtocol:    "TLSv1.2",
				TargetGroupArn: "arn:aws:elasticloadbalancing:us-west-2:123456789012:targetgroup/my-targets/bcdef1234567890a",
				TraceId:        `Root=1-58337327-72bd00b0343d75b906739c42`,
			},
		},
		{
			name: "Undocumented: target group name given as target",
			line: `h2 2017-01-24T08:45:52.892957Z app/service-with-a-healthcheck/e2e0a602efeb2ce8 10.124.148.175:40203 target-group-for-service-with-a-healthcheck-6457d69767c267f3 -1 -1 -1 502 502 104 241 "GET https://service.with.a.healthcheck.com:443/healthcheck HTTP/2.0" "UserAgent2 (UserAgent: 4a2435f2e5e1:us-east-1)" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 arn:aws:elasticloadbalancing:ap-southeast-2:123456789012:targetgroup/target-group-for-service-with-a-healthcheck/6457d69767c267f3 "Root=1-58871440-5a517d6200e61ddf01f2dfcb"`,
			expected: &LogLineEntry{
				ConnectionType:         "h2",
				Timestamp:              "2017-01-24T08:45:52.892957Z",
				Client:                 "10.124.148.175",
				LoadBalancerStatusCode: "502",
				TargetStatusCode:       "502",
				Bytes:                  345,
				ReceivedBytes:          104,
				SentBytes:              241,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         "https://service.with.a.healthcheck.com:443/healthcheck",
					HttpVersion: "HTTP/2.0",
				},
				UserAgent:      "UserAgent2 (UserAgent: 4a2435f2e5e1:us-east-1)",
				TlsCipher:      "ECDHE-RSA-AES128-GCM-SHA256",
				TlsProtocol:    "TLSv1.2",
				TargetGroupArn: "arn:aws:elasticloadbalancing:ap-southeast-2:123456789012:targetgroup/target-group-for-service-with-a-healthcheck/6457d69767c267f3",
				TraceId:        `Root=1-58871440-5a517d6200e61ddf01f2dfcb`,
			},
		},
		{
			name: "Undocumented: target group arn omitted without placeholder hypen",
			line: `https 2017-02-15T23:22:57.569871Z app/service-with-a-healthcheck-com/ec9b93e19edde575 180.97.106.162:5377 - -1 -1 -1 400 - 0 321 "- http://service-with-a-healthcheck-com.ap-southeast-2.elb.amazonaws.com:443- -" "-" - -  "-"`,
			expected: &LogLineEntry{
				ConnectionType:         "https",
				Timestamp:              "2017-02-15T23:22:57.569871Z",
				Client:                 "180.97.106.162",
				LoadBalancerStatusCode: "400",
				Bytes:         321,
				ReceivedBytes: 0,
				SentBytes:     321,
				Request: LogLineEntryRequest{
					Client: "Transport",
					Url:    "http://service-with-a-healthcheck-com.ap-southeast-2.elb.amazonaws.com:443-",
				},
			},
		},
		{
			name: "User agent contains escaped quotes",
			line: `2017-02-10T04:52:10.808825Z other-service-with-images-com 104.192.142.137:38550 10.116.45.22:8080 0.000064 0.002673 0.000024 304 304 0 0 "GET https://other.service.with.images.com/img/skelebot_icon.svg HTTP/1.1" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) UserAgent/Optional(\"985\") (modern)" ECDHE-RSA-AES128-SHA256 TLSv1.2`,
			expected: &LogLineEntry{
				Timestamp:              "2017-02-10T04:52:10.808825Z",
				Client:                 "104.192.142.137",
				Target:                 "10.116.45.22",
				RequestProcessingTime:  0.000064,
				TargetProcessingTime:   0.002673,
				ResponseProcessingTime: 0.000024,
				LoadBalancerStatusCode: "304",
				TargetStatusCode:       "304",
				Duration:               2.761,
				Port:                   8080,
				ReceivedBytes:          0,
				SentBytes:              0,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         "https://other.service.with.images.com/img/skelebot_icon.svg",
					HttpVersion: "HTTP/1.1",
				},
				UserAgent:   `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) UserAgent/Optional(\"985\") (modern)`,
				TlsCipher:   "ECDHE-RSA-AES128-SHA256",
				TlsProtocol: "TLSv1.2",
			},
		},
		{
			name: "Request url contains unescaped quotes",
			line: `2017-02-19T14:41:03.607756Z my-identity-service 127.0.0.1:12345 127.0.0.1:8081 0.000039 0.000836 0.000025 301 301 0 178 "GET http://ec2-100-100-100-100.compute-1.amazonaws.com:80/login.cgi?login=OpenVAS"><script>alert(/openvas-xss-test/)</script> HTTP/1.1" "Mozilla/5.0 [en] (X11, U; OpenVAS 8.0.8)" - -`,
			expected: &LogLineEntry{
				Timestamp:              "2017-02-19T14:41:03.607756Z",
				Client:                 "127.0.0.1",
				Target:                 "127.0.0.1",
				RequestProcessingTime:  0.000039,
				TargetProcessingTime:   0.000836,
				ResponseProcessingTime: 0.000025,
				LoadBalancerStatusCode: "301",
				TargetStatusCode:       "301",
				Duration:               0.9,
				Port:                   8081,
				Bytes:                  178,
				ReceivedBytes:          0,
				SentBytes:              178,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         `http://ec2-100-100-100-100.compute-1.amazonaws.com:80/login.cgi?login=OpenVAS"><script>alert(/openvas-xss-test/)</script>`,
					HttpVersion: "HTTP/1.1",
				},
				UserAgent: `Mozilla/5.0 [en] (X11, U; OpenVAS 8.0.8)`,
			},
		},
		{
			name: "Empty strings: request url, target group, trace id",
			line: `https 2017-02-15T23:22:57.569871Z app/service-with-a-healthcheck-com/ec9b93e19edde575 180.97.106.162:5377 - -1 -1 -1 400 - 0 321 "- http://service-with-a-healthcheck-com.ap-southeast-2.elb.amazonaws.com:443- " "" - -  ""`,
			expected: &LogLineEntry{
				ConnectionType:         "https",
				Timestamp:              "2017-02-15T23:22:57.569871Z",
				Client:                 "180.97.106.162",
				LoadBalancerStatusCode: "400",
				Bytes:         321,
				ReceivedBytes: 0,
				SentBytes:     321,
				Request: LogLineEntryRequest{
					Client: "Transport",
					Url:    "http://service-with-a-healthcheck-com.ap-southeast-2.elb.amazonaws.com:443-",
				},
			},
		},
		{
			name:      "Missing load balancer name field",
			line:      `2015-05-13T23:39:43.945958Z 192.168.131.39:2817 10.0.0.1:80 0.001069 0.000028 0.000041 - - 82 305 "- - - " "-" - -`,
			expectErr: true,
		},
		{
			name: "ELB parse log line simple test",
			line: `2017-08-03T06:14:58.663865Z abcde-ELB-AR7B792UTLYD 127.0.0.1:54321 192.168.0.1:32415 0.00004 0.002092 0.00002 200 200 0 1233 "GET https://example.com:443/version HTTP/1.1" "curl/7.54.0" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2`,
			expected: &LogLineEntry{
				UserAgent:              "curl/7.54.0",
				TlsCipher:              "ECDHE-RSA-AES128-GCM-SHA256",
				TlsProtocol:            "TLSv1.2",
				Timestamp:              "2017-08-03T06:14:58.663865Z",
				Client:                 "127.0.0.1",
				LoadBalancerStatusCode: "200",
				Duration:               2.152,
				Port:                   32415,
				Bytes:                  1233,
				ReceivedBytes:          0,
				SentBytes:              1233,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         "https://example.com:443/version",
					HttpVersion: "HTTP/1.1",
				},
				Target:                 "192.168.0.1",
				TargetStatusCode:       "200",
				RequestProcessingTime:  0.00004,
				TargetProcessingTime:   0.002092,
				ResponseProcessingTime: 0.00002,
			},
		},
		{
			name: "ALB SSL/TLS with Trace ID and trailing whitespace",
			line: `https 2017-08-28T05:44:38.985373Z app/my-app/abcdefg 10.0.0.1:43408 10.0.0.2:8080 0.000 0.003 0.000 200 200 199 210 "GET https://my-app.example.com:443/health HTTP/1.1" "curl/7.46.0" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 arn:aws:elasticloadbalancing:us-west-2:123456789012:targetgroup/my-targets/abcdefg1234 "Root=1-abcd1234-7860da6f14ec5f1903d352bd" `, // trailing whitespace!
			expected: &LogLineEntry{
				ConnectionType:         "https",
				Timestamp:              "2017-08-28T05:44:38.985373Z",
				Client:                 "10.0.0.1",
				Target:                 "10.0.0.2",
				RequestProcessingTime:  0.000,
				TargetProcessingTime:   0.003,
				ResponseProcessingTime: 0.000,
				LoadBalancerStatusCode: "200",
				TargetStatusCode:       "200",
				Duration:               3,
				Port:                   8080,
				Bytes:                  409,
				ReceivedBytes:          199,
				SentBytes:              210,
				Request: LogLineEntryRequest{
					Client:      "HTTP",
					Method:      "GET",
					Url:         "https://my-app.example.com:443/health",
					HttpVersion: "HTTP/1.1",
				},
				UserAgent:      "curl/7.46.0",
				TlsCipher:      "ECDHE-RSA-AES128-GCM-SHA256",
				TlsProtocol:    "TLSv1.2",
				TargetGroupArn: "arn:aws:elasticloadbalancing:us-west-2:123456789012:targetgroup/my-targets/abcdefg1234",
				TraceId:        `Root=1-abcd1234-7860da6f14ec5f1903d352bd`,
			},
		},
	}

	parser := &LoadBalancerLogLineParser{}
	for _, test := range parseTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			actual, err := parser.ParseLine(test.line)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, test.expected, actual)
			}
		})
	}
}

func TestParseInt64FromMapKey(t *testing.T) {
	t.Parallel()
	m := map[string]string{
		"abc": "1234567890123",
	}
	assert.Equal(t, int64(1234567890123), parseInt64FromMapKey(m, "abc"))
}

func TestParseFloat64FromMapKey(t *testing.T) {
	t.Parallel()
	m := map[string]string{
		"abc": "1234567.890123",
	}
	assert.Equal(t, float64(1234567.890123), parseFloat64FromMapKey(m, "abc"))
}
