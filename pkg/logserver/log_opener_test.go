package logserver

import (
	"context"
	"errors"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mockSqsDelete struct {
	t             *testing.T
	expectedInput *sqs.DeleteMessageInput
	err           error
}

func (s *mockSqsDelete) ReceiveMessage(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	return nil, nil
}

func (s *mockSqsDelete) DeleteMessage(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	require.Equal(s.t, s.expectedInput, input)
	return nil, s.err
}

func TestLogOpenerSuccessReturnsContextError(t *testing.T) {
	logOpener := &LogOpener{
		SqsService: &mockSqsDelete{
			t: t,
			expectedInput: &sqs.DeleteMessageInput{
				QueueUrl:      aws.String("queue"),
				ReceiptHandle: aws.String("receipt"),
			},
			err: errors.New("context done"),
		},
		QueueUrl:      "queue",
		ReceiptHandle: "receipt",
	}
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	assert.Error(t, logOpener.Success(ctx))
}
