package logserver

import (
	"bytes"
	"compress/gzip"
	"context"
	"errors"
	"io"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mockS3 struct {
	t             *testing.T
	expectedInput *s3.GetObjectInput
	reader        io.ReadCloser
	err           error
}

func (s *mockS3) GetObject(ctx context.Context, input *s3.GetObjectInput) (*s3.GetObjectOutput, error) {
	if s.expectedInput != nil {
		require.Equal(s.t, s.expectedInput, input)
	}
	return &s3.GetObjectOutput{Body: s.reader}, s.err
}

type mockTagFetcher struct {
	t                 *testing.T
	expectedRegion    string
	expectedAccountId string
	expectedName      string
	elbTags           map[string]string
	elbv2Tags         map[string]string
	err               error
}

func (f *mockTagFetcher) FetchTagsElb(ctx context.Context, name string) (map[string]string, error) {
	require.Equal(f.t, f.expectedName, name)
	return f.elbTags, f.err
}

func (f *mockTagFetcher) FetchTagsElbv2(ctx context.Context, region string, accountId string, name string) (map[string]string, error) {
	require.Equal(f.t, f.expectedRegion, region)
	require.Equal(f.t, f.expectedAccountId, accountId)
	require.Equal(f.t, f.expectedName, name)
	return f.elbv2Tags, f.err
}

type stubReadCloser struct {
	id     string
	reader io.Reader
}

func (r *stubReadCloser) Read(p []byte) (int, error) {
	if r.reader == nil {
		return 0, nil
	}
	return r.reader.Read(p)
}

func (r *stubReadCloser) Close() error {
	return nil
}

func TestOpenErrorsOnInvalidFilename(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	opener := &S3LogFileOpener{
		filename: "invalidfilename",
	}

	_, err := opener.Open(ctx, &LogFileInfo{})
	assert.Error(t, err)
}

func TestOpenReturnsCorrectLogFileInfo(t *testing.T) {
	t.Parallel()

	logInfoTests := []struct {
		name         string
		fetcher      *mockTagFetcher
		bucket       string
		filename     string
		expectedInfo LogFileInfo
	}{
		{
			"ELB tags",
			&mockTagFetcher{
				expectedName: "my-loadbalancer",
				elbTags: map[string]string{
					"color": "green",
				},
				err: nil,
			},
			"my-loadbalancer-logs",
			"my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
			LogFileInfo{
				LoadBalancer: LogEntryLoadBalancer{
					AccountId: "123456789012",
					Name:      "my-loadbalancer",
					Type:      "ELB",
					Ip:        "172.160.001.192",
					Region:    "us-west-2",
					Tags: map[string]string{
						"color": "green",
					},
				},
				LogBucket:   "my-loadbalancer-logs",
				LogFilename: "my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
			},
		},
		{
			"ALB tags",
			&mockTagFetcher{
				expectedRegion:    "us-west-2",
				expectedAccountId: "123456789012",
				expectedName:      "my-loadbalancer",
				elbv2Tags: map[string]string{
					"company": "atlassian",
				},
				err: nil,
			},
			"my-bucket",
			"my-bucket/prefix/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2016/05/01/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log.gz",
			LogFileInfo{
				LoadBalancer: LogEntryLoadBalancer{
					AccountId: "123456789012",
					Name:      "my-loadbalancer",
					Type:      "ALB",
					Ip:        "172.160.001.192",
					Region:    "us-west-2",
					Tags: map[string]string{
						"company": "atlassian",
					},
				},
				LogBucket:   "my-bucket",
				LogFilename: "my-bucket/prefix/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2016/05/01/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log.gz",
			},
		},
		{
			"Omits tags when error fetching tags",
			&mockTagFetcher{
				expectedName: "my-loadbalancer",
				elbTags:      nil,
				err:          errors.New("error fetching tags"),
			},
			"my-loadbalancer-logs",
			"my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
			LogFileInfo{
				LoadBalancer: LogEntryLoadBalancer{
					AccountId: "123456789012",
					Name:      "my-loadbalancer",
					Type:      "ELB",
					Ip:        "172.160.001.192",
					Region:    "us-west-2",
					Tags:      nil,
				},
				LogBucket:   "my-loadbalancer-logs",
				LogFilename: "my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
			},
		},
	}

	for _, test := range logInfoTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.fetcher.t = t

			reader := &stubReadCloser{}
			if test.expectedInfo.LoadBalancer.Type == "ALB" {
				var err error
				reader.reader, err = rawGzippedReaderFromString("somestring")
				require.NoError(t, err)
			}
			opener := &S3LogFileOpener{
				bucket:   test.bucket,
				filename: test.filename,
				s3Service: &mockS3{
					t:      t,
					reader: reader,
					err:    nil,
				},
				tagFetcher: test.fetcher,
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			var logFileInfo LogFileInfo
			_, err := opener.Open(ctx, &logFileInfo)
			require.NoError(t, err)
			assert.Equal(t, test.expectedInfo, logFileInfo)
		})
	}
}

func TestOpenReturnsElbLogReader(t *testing.T) {
	t.Parallel()

	expectedReadCloser := &stubReadCloser{id: "a reader"}
	fileOpener := &S3LogFileOpener{
		bucket:   "my-loadbalancer-logs",
		filename: "my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
		s3Service: &mockS3{
			t: t,
			expectedInput: &s3.GetObjectInput{
				Bucket: aws.String("my-loadbalancer-logs"),
				Key:    aws.String("my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log"),
			},
			reader: expectedReadCloser,
			err:    nil,
		},
		tagFetcher: &mockTagFetcher{
			expectedName: "my-loadbalancer",
		},
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	readCloser, err := fileOpener.Open(ctx, &LogFileInfo{})
	require.NoError(t, err)
	assert.Equal(t, expectedReadCloser, readCloser)
}

func TestOpenHandlesS3Error(t *testing.T) {
	t.Parallel()

	fileOpener := &S3LogFileOpener{
		bucket:   "my-loadbalancer-logs",
		filename: "my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
		s3Service: &mockS3{
			t: t,
			expectedInput: &s3.GetObjectInput{
				Bucket: aws.String("my-loadbalancer-logs"),
				Key:    aws.String("my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log"),
			},
			reader: nil,
			err:    errors.New("GetObject error"),
		},
		tagFetcher: &mockTagFetcher{
			expectedName: "my-loadbalancer",
		},
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_, err := fileOpener.Open(ctx, &LogFileInfo{})
	assert.Error(t, err)
}

func TestParseLogFilename(t *testing.T) {
	t.Parallel()
	parseTests := []struct {
		name      string
		filename  string
		expected  LogEntryLoadBalancer
		expectErr bool
	}{
		{
			name:     "Parse ALB log filename",
			filename: "my-bucket/prefix/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2016/05/01/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log.gz",
			expected: LogEntryLoadBalancer{
				AccountId: "123456789012",
				Name:      "my-loadbalancer",
				Type:      "ALB",
				Ip:        "172.160.001.192",
				Region:    "us-west-2",
			},
		},
		{
			name:     "Parse ELB log filename",
			filename: "my-loadbalancer-logs/my-app/AWSLogs/123456789012/elasticloadbalancing/us-west-2/2014/02/15/123456789012_elasticloadbalancing_us-west-2_my-loadbalancer_20140215T2340Z_172.160.001.192_20sg8hgm.log",
			expected: LogEntryLoadBalancer{
				AccountId: "123456789012",
				Name:      "my-loadbalancer",
				Type:      "ELB",
				Ip:        "172.160.001.192",
				Region:    "us-west-2",
			},
		},
		{
			name:      "Handle invalid log filename",
			filename:  "invalid/filename/here.log.gz",
			expectErr: true,
		},
	}

	for _, test := range parseTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			var actual LogEntryLoadBalancer
			err := parseLogFilename(test.filename, &actual)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, test.expected, actual)
			}
		})
	}
}

func rawGzippedReaderFromString(s string) (io.Reader, error) {
	var buffer bytes.Buffer
	writer := gzip.NewWriter(&buffer)
	if _, err := writer.Write([]byte(s)); err != nil {
		return nil, err
	}
	if err := writer.Close(); err != nil {
		return nil, err
	}
	return &buffer, nil
}
