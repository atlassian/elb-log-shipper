package logserver

import (
	"context"
	"fmt"
	"regexp"
)

// Maps group names to matching substrings when matching s against regex. The empty string maps to the entire string, as
// per convention. Returns nil if s doesn't match regex.
func findNamedStringSubmatches(regex *regexp.Regexp, s string) map[string]string {
	submatches := regex.FindStringSubmatch(s)
	if submatches == nil {
		return nil
	}

	matches := make(map[string]string)
	for i, name := range regex.SubexpNames() {
		matches[name] = submatches[i]
	}
	return matches
}

func ContextOrError(ctx context.Context, format string, a ...interface{}) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return fmt.Errorf(format, a...)
	}
}
