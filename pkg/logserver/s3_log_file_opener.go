package logserver

import (
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	logFilenameFormat = `^.+/(?P<aws_account_id>[^/]+)_elasticloadbalancing_(?P<load_balancer_region>[^_]+)_(?P<load_balancer_name>[^_]+)_.+Z_(?P<load_balancer_ip>\d+\.\d+\.\d+\.\d+)_.+\.log(?P<gzipped>\.gz)?$`
)

var (
	logFilenameRegex = regexp.MustCompile(logFilenameFormat)
)

type S3LogFileOpener struct {
	bucket     string
	filename   string
	size       int64
	s3Service  S3
	tagFetcher TagFetcher
}

func (o *S3LogFileOpener) Open(ctx context.Context, logFileInfo *LogFileInfo) (io.ReadCloser, error) {
	*logFileInfo = LogFileInfo{
		LogBucket:   o.bucket,
		LogFilename: o.filename,
	}

	err := parseLogFilename(o.filename, &logFileInfo.LoadBalancer)
	if err != nil {
		return nil, fmt.Errorf("error parsing log filename %s: %v", o.filename, err)
	}

	if logFileInfo.LoadBalancer.Type == "ALB" {
		logFileInfo.LoadBalancer.Tags, err = o.tagFetcher.FetchTagsElbv2(ctx, logFileInfo.LoadBalancer.Region,
			logFileInfo.LoadBalancer.AccountId, logFileInfo.LoadBalancer.Name)
		if err != nil {
			select {
			case <-ctx.Done():
				return nil, err
			default:
			}
			log.WithFields(log.Fields{
				"load_balancer_name": logFileInfo.LoadBalancer.Name,
			}).Warningf("error fetching tags for ALB: %v", err)
		}
	} else {
		if logFileInfo.LoadBalancer.Tags, err = o.tagFetcher.FetchTagsElb(ctx, logFileInfo.LoadBalancer.Name); err != nil {
			select {
			case <-ctx.Done():
				return nil, err
			default:
			}
			log.WithFields(log.Fields{
				"load_balancer_name": logFileInfo.LoadBalancer.Name,
			}).Warningf("error fetching tags for ELB: %v", err)
		}
	}
	inputFile, err := o.s3Service.GetObject(ctx, &s3.GetObjectInput{
		Bucket: aws.String(o.bucket),
		Key:    aws.String(o.filename),
	})
	if err != nil {
		return nil, ContextOrError(ctx, "error reading file (bucket=%s, key=%s) from s3: %v", o.bucket, o.filename, err)
	}

	if logFileInfo.LoadBalancer.Type != "ALB" {
		return inputFile.Body, nil
	}

	var unzippedReader *gzip.Reader
	if unzippedReader, err = gzip.NewReader(inputFile.Body); err != nil {
		return nil, fmt.Errorf("error un-gzipping logfile: %v", err)
	}
	return unzippedReader, nil
}

func parseLogFilename(filename string, loadBalancerEntry *LogEntryLoadBalancer) error {
	filenameSubmatches := findNamedStringSubmatches(logFilenameRegex, filename)
	if filenameSubmatches == nil {
		return errors.New("non conforming log filename")
	}

	*loadBalancerEntry = LogEntryLoadBalancer{
		AccountId: filenameSubmatches["aws_account_id"],
		Name:      strings.Replace(filenameSubmatches["load_balancer_name"], ".", "/", -1),
		Ip:        filenameSubmatches["load_balancer_ip"],
		Region:    filenameSubmatches["load_balancer_region"],
	}
	if strings.HasSuffix(filename, ".gz") {
		loadBalancerEntry.Type = "ALB"
	} else {
		loadBalancerEntry.Type = "ELB"
	}
	return nil
}
